document.addEventListener("DOMContentLoaded", function () {

    var btnMenu = document.querySelector('.btn-menu');
    var nav = document.querySelector('.main-nav');

    btnMenu.addEventListener('click', function (e) {
        nav.classList.toggle('open');
        btnMenu.querySelector('i').classList.toggle('fa-window-close');
        btnMenu.querySelector('i').classList.toggle('fa-bars');

    });
});
